# Die MicroCell Architektur

*dieses Dokument wird unter folgenden Link gespeichert: https://gitlab.com/ourplant.net/ourplant_os/documents/Dokumentation/-/tree/main/Examples*

## Prototyp

### Variante Demoaufbau (Tutorial Projekt)

**Ziele:**
* Verfeinerung der Dokumentation
    * Definitionen der einzelnen Elemente (z.B. Discovery-Manager, Skill-Interfaces, Zelle, etc.)
    * Definitionen werden in GitLab-Repo als .md abgelegt
    * Zur Umsetzung der MicroCell Architektur wird das **TCellObject** als Basisobjekt bzw. Stammzelle eingeführt. Das TCellObject implementiert das Basis-Skillinterface **IsiCellObject**. 
* Erarbeitung eines HowTo
* Grundlage Baumstruktur, Recrusiv für Discoverymanager
* Lesbarkeit zu verbessern
* Kommunikation
* Zellstruktur
* Interfaces
* Datenstruktur
* Verschiedene Sprache: Delphi, Python, Java(Markus Bock)
* 

#### Umfang
*  Rootzelle Interface + Klasse repräsentationen als (json)
*  ein Skill Interface (Addition(cell, cell))
*  1 subzelle ``<integer>`` -> eingabe repräsentationen als (int, float, json)
*  1 subzelle ``<string>`` muss konvertierbar gemacht werden -> konvertierbar
*  konsole sucht rootzelle
*  konsole sucht alle childs(2)
*  input -> childs
*  konsole sucht sich den addition-skill
*  zugriff auf den skill 
*  Rootzelle managed die addition()+
*  http als rest endpoint 
*  performance? 


#### Baumstruktur
* root
    - Cell
    -  


### Python 

```python
"""
__main__.py

entrypoint script
"""
https://im-coder.com/ausfuehren-von-python-code-in-markdown.html


```


### Variante Conveyor

* Verifikation am realen/produktiven System
    * Komplizierte Baumstruktur
    * Interaktion mit Hardware
* Prototyp = Conveyer auf Grundplatte X3 in Halle-2
    * TODO: mechanischer Umbau um auf aktuellen Stand zu kommen
* ProzessCell -> ConveyorCell -> #X ShuttleCells -> #X ComponentCells
* 



------

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>

Dieses Werk **OurPlant MicroCell Architektur** von [Häcker Automation GmbH](https://www.haecker-automation.de/) ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 4.0 International Lizenz</a>

Die Quellen dieser Arbeiten finden sich auf [Gitlab](https://gitlab.com/ourplant.net/software/microcell-architecture/Dokumentation)
